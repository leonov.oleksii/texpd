import logging
import os
import time

import camelot
from flask import Flask, jsonify
from waitress import serve

app = Flask(__name__)


@app.route('/pdf/tables/<file_id>')
def get_pdf_tables(file_id):
    pdf_file = app.config.base_pdf_url + file_id
    before = time.time()
    camelot_tables = camelot.read_pdf(pdf_file)
    app.logger.info('Read PDF tables for %s in %d ms', pdf_file, 1000 * (time.time() - before))
    tables = list(map(convert_table, camelot_tables))
    return jsonify(tables)


def convert_table(table):
    result = {'columns': []}
    for column in table.df:
        column_values = list(map(clean_newlines, filter(None, table.df[column])))
        if column_values:
            column_result = {'name': column_values.pop(0), 'data': column_values}
            result['columns'].append(column_result)
    return result


def clean_newlines(column_value):
    return column_value.replace('\n', ' ').replace('\r', '')


def configure_logging():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s  %(levelname)s %(process)d [%(threadName)s] %(name)s : %(message)s",
        handlers=[
            logging.FileHandler(os.path.join('log', 'table-parser.log')),
            logging.StreamHandler()
        ]
    )
    logging.getLogger('pdfminer').setLevel(logging.WARNING)
    logging.getLogger('camelot').setLevel(logging.WARNING)


def configure_app():
    app.config.base_pdf_url = os.environ.get("BASE_PDF_URL") or 'http://localhost:8080/out/files/'
    app.config.port = os.environ.get("HTTP_PORT") or 5050


if __name__ == '__main__':
    configure_logging()
    configure_app()
    serve(app, host='0.0.0.0', port=app.config.port)
