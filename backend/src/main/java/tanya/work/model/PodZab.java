package tanya.work.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class PodZab<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = -3728547831826141550L;

    private List<T> pod = new ArrayList<>();
    private List<T> zab = new ArrayList<>();
}
