package tanya.work.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "checksum")
@ToString(of = {"number", "type"})
public class Vedomost implements HasChecksum, Serializable {
    @Serial
    private static final long serialVersionUID = -1688063927326821723L;
    private String number;
    private String type;
    private boolean podZab;
    private LocalDate date;
    private BigDecimal price;
    private String checksum;
    private PodZab<String> pamyatkas;

    public Vedomost(PdfFile file) {
        this.checksum = file.getChecksum();
    }
}
