package tanya.work.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@ToString(of = {"number", "type"})
@EqualsAndHashCode(of = "checksum")
public class Pamyatka implements HasChecksum, Serializable {
    @Serial
    private static final long serialVersionUID = -5402939849126223374L;
    private String number;
    private String type;
    private PodZabType podZabType;
    private String checksum;
    private List<String> vagonNumbers;

    public Pamyatka(PdfFile pdfFile) {
        this.checksum = pdfFile.getChecksum();
    }
}
