package tanya.work.model;

import com.google.common.base.Preconditions;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "checksum")
@ToString(of = "name")
public class PdfFile implements HasChecksum, Serializable {
    @Serial
    private static final long serialVersionUID = 4789326523805953865L;

    private String name;
    private String checksum;
    private List<String> textContent = Collections.emptyList();

    public PdfFile(RawFile rawFile) {
        Preconditions.checkArgument(rawFile.isPdf(), "File is not PDF");
        this.name = rawFile.getName();
        this.checksum = rawFile.getChecksum();
    }
}
