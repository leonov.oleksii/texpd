package tanya.work.model;

public enum VagonTableColumn {
    N_PP,
    N_VAGON,
    FIX_TYPE,
    OWNER,
    YEAR_BUILT,
    NOTES
}
