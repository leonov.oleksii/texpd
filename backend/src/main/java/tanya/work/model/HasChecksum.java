package tanya.work.model;

public interface HasChecksum {
    String getChecksum();
}
