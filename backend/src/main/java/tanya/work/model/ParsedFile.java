package tanya.work.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "checksum")
@ToString(of = "name")
public class ParsedFile implements HasChecksum, Serializable {
    @Serial
    private static final long serialVersionUID = 578762051525158224L;
    private String checksum;
    private String name;
    private String contentUri;

    public ParsedFile(RawFile rawFile) {
        this.checksum = rawFile.getChecksum();
        this.contentUri = rawFile.getStorageUri();
        this.name = rawFile.getName();
    }
}
