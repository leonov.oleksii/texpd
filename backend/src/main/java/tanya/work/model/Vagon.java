package tanya.work.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;

@Data
@ToString(of = {"number", })
@EqualsAndHashCode(of = {"number", "owner"})
public class Vagon implements Serializable {
    @Serial
    private static final long serialVersionUID = -2581428754331672978L;

    private int numPp;
    private String number;
    private String owner;
    private String fixType;
    private int yearBuilt;
    private String notes;
    private String sourceFile;
}
