package tanya.work.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import tanya.work.util.ChecksumUtil;

import java.util.function.Function;


@Getter
@ToString(of = {"name", "storageUri"})
@EqualsAndHashCode(of = "checksum")
public class RawFile implements HasChecksum {

    private static final String PDF = "pdf";
    private static final String[] EXCEL = {"xls", "xlsx"};

    private final byte[] content;
    private final String contentType;
    private final String name;
    private final String storageUri;
    private String checksum;
    private final Function<byte[], String> checksumFunc;

    public RawFile(byte[] content, String contentType, String name, String storageUri,
                   Function<byte[], String> checksumFunc) {
        this.content = content;
        this.contentType = contentType;
        this.name = name;
        this.storageUri = storageUri;
        this.checksumFunc = checksumFunc;
    }

    public RawFile(byte[] content, String contentType, String name, String storageUri) {
        this(content, contentType, name, storageUri, ChecksumUtil::getChecksum);
    }

    public String getExtension() {
        return StringUtils.lowerCase(FilenameUtils.getExtension(name));
    }

    public boolean isPdf() {
        return PDF.equals(getExtension());
    }

    public boolean isExcel() {
        return ArrayUtils.contains(EXCEL, getExtension());
    }

    public String getChecksum() {
        if (StringUtils.isEmpty(checksum)) {
            checksum = checksumFunc.apply(content);
        }
        return checksum;
    }
}
