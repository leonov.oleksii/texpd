package tanya.work.util;

import java.math.BigDecimal;
import java.util.function.IntConsumer;

public final class NumberUtils {
    private NumberUtils() {

    }

    public static String doubleToPlainString(double value) {
        return BigDecimal.valueOf(value).stripTrailingZeros().toPlainString();
    }

    public static void parseInt(String value, IntConsumer setter) {
        try {
            setter.accept(Integer.parseInt(value));
        } catch (NumberFormatException ignored) {
        }
    }
}
