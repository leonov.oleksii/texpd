package tanya.work.util.cq;

import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;
import tanya.work.model.HasChecksum;

import java.util.function.Function;

public class FunctionalAttribute<O, A> extends SimpleAttribute<O, A> {

    private final Function<O, A> attrFunction;

    public FunctionalAttribute(Class<O> objectType, Class<A> attributeType,
                               Function<O, A> attrFunction,
                               String attributeName) {
        super(objectType, attributeType, attributeName);
        this.attrFunction = attrFunction;
    }

    public static <T extends HasChecksum> FunctionalAttribute<T, String> checksumAttr(Class<T> objectType) {
        return new FunctionalAttribute<>(objectType, String.class, HasChecksum::getChecksum, "checksum");
    }

    @Override
    public A getValue(O object, QueryOptions queryOptions) {
        return attrFunction.apply(object);
    }

    @Override
    public String toString() {
        return "FunctionalAttribute{" + "name=" + getAttributeName() + '}';
    }
}
