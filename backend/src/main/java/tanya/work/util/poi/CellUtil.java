package tanya.work.util.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import static tanya.work.util.NumberUtils.doubleToPlainString;
import static tanya.work.util.StringUtils.trimEverywhere;

public final class CellUtil {
    private CellUtil() {}

    public static String getCellValue(Cell cell) {
        String result = null;
        if (null != cell) {
            if (CellType.NUMERIC.equals(cell.getCellType())) {
                result = doubleToPlainString(cell.getNumericCellValue());
            } else if (CellType.STRING.equals(cell.getCellType())) {
                result = trimEverywhere(cell.getStringCellValue());
            }
        }
        return result;
    }
}
