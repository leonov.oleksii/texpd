package tanya.work.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class HostnameUtil {
    public static final String HOSTNAME = getHostname();
    private HostnameUtil() {
    }

    private static String getHostname() {
        String result;
        try {
            result = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            result = "unknown";
        }
        return result;
    }
}
