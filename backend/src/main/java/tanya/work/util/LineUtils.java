package tanya.work.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class LineUtils {
    private static final Pattern DIGITS_1_GROUP = Pattern.compile("\\D*?(\\d+)\\D*");
    private LineUtils() {
    }

    public static String extractOneNumber(String line) {
        String result = null;
        Matcher matcher = DIGITS_1_GROUP.matcher(line);
        if (matcher.matches()) {
            result = matcher.group(1);
        }
        return result;
    }

    public static String getLine(List<String> lines, int index) {
        return index < lines.size() ? lines.get(index) : "";
    }
}
