package tanya.work.util;

import com.google.common.base.Preconditions;

public final class StringUtils {
    private StringUtils() {}

    public static String safeReplace(String string, String regex, String replacement) {
        Preconditions.checkNotNull(regex);
        Preconditions.checkNotNull(replacement);
        String result = string;
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(string)) {
            result = string.replaceAll(regex ,replacement);
        }
        return result;
    }

    public static String trimEverywhere(String string) {
        String trimmed = org.apache.commons.lang3.StringUtils.trim(string);
        return safeReplace(trimmed, "\\s+", " ");
    }

}
