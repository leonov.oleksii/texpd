package tanya.work.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;
import java.util.Base64;

public final class ChecksumUtil {
    private static final MessageDigest SHA_3_256 = DigestUtils.getSha3_256Digest();
    private static final Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
    private ChecksumUtil() {}

    public static String getChecksum(byte[] content) {
        //Hex.encodeHexString
        return encoder.encodeToString(SHA_3_256.digest(content));
    }
}
