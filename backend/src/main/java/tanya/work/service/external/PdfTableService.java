package tanya.work.service.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import tanya.work.dto.PdfTableDto;

import java.util.List;

@FeignClient(value = "pdfTables", url = "${urls.pdfTablesService}")
public interface PdfTableService {
    @GetMapping("/pdf/tables/{fileId}")
    List<PdfTableDto> getTablesFromPdf(@PathVariable String fileId);
}
