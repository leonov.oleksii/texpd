package tanya.work.service;

import tanya.work.dto.PdfTableDto;
import tanya.work.model.RawFile;

import java.util.List;

public interface PdfTableFacade {
    List<PdfTableDto> getPdfTables(RawFile rawFile);
}
