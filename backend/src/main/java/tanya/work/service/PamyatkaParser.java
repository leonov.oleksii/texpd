package tanya.work.service;

import tanya.work.model.Pamyatka;
import tanya.work.model.PdfFile;
import tanya.work.model.RawFile;

import java.util.Optional;

public interface PamyatkaParser {
    Optional<Pamyatka> parse(PdfFile pdfFile, RawFile rawFile);
}
