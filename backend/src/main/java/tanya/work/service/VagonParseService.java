package tanya.work.service;

import tanya.work.model.RawFile;
import tanya.work.model.Vagon;

import java.util.Set;

public interface VagonParseService {
    Set<Vagon> parse(RawFile excelFile);
}
