package tanya.work.service;

import org.apache.poi.ss.usermodel.Sheet;
import tanya.work.service.impl.VagonTableHeader;

import java.util.Optional;

public interface VagonTableHeaderFinder {
    Optional<VagonTableHeader> findVagonTableHeader(Sheet sheet);
}
