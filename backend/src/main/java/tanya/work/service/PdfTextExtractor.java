package tanya.work.service;

import tanya.work.model.RawFile;

import java.util.List;

public interface PdfTextExtractor {
    List<String> extractText(RawFile file);
}
