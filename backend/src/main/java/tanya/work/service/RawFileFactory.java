package tanya.work.service;

import org.springframework.web.multipart.MultipartFile;
import tanya.work.model.RawFile;

public interface RawFileFactory {
    RawFile create(MultipartFile file, String sessionId);
}
