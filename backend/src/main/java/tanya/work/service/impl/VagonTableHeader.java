package tanya.work.service.impl;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tanya.work.model.VagonTableColumn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString(of = "columnIndexes")
public class VagonTableHeader {
    private final Map<VagonTableColumn, Integer> columnIndexes = new HashMap<>();
    private int rowNum;

    public void addColumnIndex(VagonTableColumn column, int index) {
        columnIndexes.put(column, index);
    }

    public boolean containsAll(List<VagonTableColumn> requiredColumns) {
        return requiredColumns.stream().allMatch(columnIndexes::containsKey);
    }
}
