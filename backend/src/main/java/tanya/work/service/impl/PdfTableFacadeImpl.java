package tanya.work.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tanya.work.dto.PdfTableDto;
import tanya.work.model.RawFile;
import tanya.work.repo.RawFileRepository;
import tanya.work.service.PdfTableFacade;
import tanya.work.service.external.PdfTableService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PdfTableFacadeImpl implements PdfTableFacade {
    private final PdfTableService pdfTableService;
    private final RawFileRepository rawFileRepository;

    @Override
    public List<PdfTableDto> getPdfTables(RawFile rawFile) {
        String checksum = rawFile.getChecksum();
        List<PdfTableDto> tableData = new ArrayList<>();
        rawFileRepository.saveFor(rawFile, () -> {
            List<PdfTableDto> response = pdfTableService.getTablesFromPdf(checksum);
            tableData.addAll(response);
        });
        return tableData;
    }
}
