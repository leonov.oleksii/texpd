package tanya.work.service.impl;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Service;
import tanya.work.model.RawFile;
import tanya.work.service.PdfTextExtractor;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PdfTextExtractorImpl implements PdfTextExtractor {

    @Override
    public List<String> extractText(RawFile file) {
        List<String> result = Collections.emptyList();
        Preconditions.checkArgument(file.isPdf());

        try (PDDocument pdDocument = PDDocument.load(file.getContent())) {
            PDFTextStripper textStripper = new PDFTextStripper();
            String text = textStripper.getText(pdDocument);

            if (StringUtils.isNotEmpty(text)) {
                result = text.lines().filter(StringUtils::isNotBlank).collect(Collectors.toList());
            } else {
                log.warn("Got empty string from {}", file.getName());
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return result;
    }
}
