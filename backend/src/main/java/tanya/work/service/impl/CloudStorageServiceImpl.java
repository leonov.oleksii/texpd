package tanya.work.service.impl;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import tanya.work.config.StorageProperties;
import tanya.work.model.RawFile;
import tanya.work.service.CloudStorageService;
import tanya.work.util.io.OutputStreamConsumer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CloudStorageServiceImpl implements CloudStorageService, InitializingBean {

    private final StorageProperties storageProperties;
    private Storage storage;
    private ExecutorService asyncSaveExecutor;

    @Override
    public void save(RawFile file) {
        if (isAsyncSave()) {
            asyncSaveExecutor.submit(() -> doSave(file, true));
        } else {
            doSave(file, false);
        }
    }

    @Override
    public void save(String storageUri, OutputStreamConsumer streamConsumer) {
        BlobInfo blobInfo = BlobInfo.newBuilder(storageProperties.getBucketName(), storageUri).build();
        try (OutputStream outputStream = Channels.newOutputStream(storage.writer(blobInfo))) {
            streamConsumer.accept(outputStream);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public Optional<InputStream> getInputStream(String storageUri) {
        BlobId blobId = BlobId.of(storageProperties.getBucketName(), storageUri);
        return Optional.ofNullable(storage.get(blobId))
                .map(Blob::reader)
                .map(Channels::newInputStream);
    }

    private void doSave(RawFile file, boolean logException) {
        BlobInfo.Builder builder = BlobInfo.newBuilder(storageProperties.getBucketName(), file.getStorageUri());
        if (StringUtils.isNotEmpty(file.getContentType())) {
            builder.setContentType(file.getContentType());
        }
        BlobInfo blobInfo = builder.build();
        long before = System.currentTimeMillis();
        try {
            storage.create(blobInfo, file.getContent());
        } catch (RuntimeException e) {
            if (logException) {
                log.error("Exception during async save", e);
            }
        }
        log.info("Uploaded {} in {} ms", file.getStorageUri(), System.currentTimeMillis() - before);
    }

    private boolean isAsyncSave() {
        return null != asyncSaveExecutor;
    }

    @Override
    public void afterPropertiesSet() {
        storage = StorageOptions.newBuilder().setCredentials(getCredentials()).build().getService();
        int saveThreadCount = storageProperties.getSaveThreadCount();
        if (saveThreadCount > 0) {
            ThreadFactoryBuilder threadFactoryBuilder = new ThreadFactoryBuilder();
            threadFactoryBuilder.setNameFormat("storage-%d");
            asyncSaveExecutor = Executors.newFixedThreadPool(saveThreadCount, threadFactoryBuilder.build());
        }
    }

    private GoogleCredentials getCredentials() {
        try {
            List<String> keyPaths = storageProperties.getKeyPaths();
            Path existingKeyPath = keyPaths.stream()
                    .map(Paths::get)
                    .filter(Files::exists)
                    .findFirst()
                    .orElseThrow(() -> new FileNotFoundException("Cannot find GCP service account key " +
                            "in following locations: " + keyPaths));
            log.info("Found GCP service account key in {}", existingKeyPath);
            return GoogleCredentials.fromStream(Files.newInputStream(existingKeyPath));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
