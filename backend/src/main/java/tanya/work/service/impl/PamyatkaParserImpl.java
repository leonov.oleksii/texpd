package tanya.work.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import tanya.work.config.PamyatkaParsingProperties;
import tanya.work.model.Pamyatka;
import tanya.work.model.PdfFile;
import tanya.work.model.PodZabType;
import tanya.work.model.RawFile;
import tanya.work.service.PamyatkaParser;
import tanya.work.service.PamyatkaTableParser;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static tanya.work.util.LineUtils.extractOneNumber;
import static tanya.work.util.LineUtils.getLine;

@Service
@RequiredArgsConstructor
public class PamyatkaParserImpl implements PamyatkaParser {

    private final PamyatkaParsingProperties parsingProps;
    private final PamyatkaTableParser pamyatkaTableParser;

    @Override
    public Optional<Pamyatka> parse(PdfFile pdfFile, RawFile rawFile) {
        Pamyatka pamyatka = new Pamyatka(pdfFile);
        List<String> textContent = pdfFile.getTextContent();
        for (int i = 0; i < textContent.size(); i++) {
            String line = pdfFile.getTextContent().get(i);
            if (isPamyatkaNumberLine(line)) {
                pamyatka.setNumber(extractOneNumber(line));
                pamyatka.setType(getLine(textContent, i + 1));
                break;
            }
        }

        return fillIfNotEmpty(pamyatka, rawFile);
    }

    private Optional<Pamyatka> fillIfNotEmpty(Pamyatka pamyatka, RawFile rawFile) {
        Optional<Pamyatka> result = Optional.empty();
        if (isNotEmpty(pamyatka)) {
            PodZabType podZabType = getPodZabType(pamyatka.getType());
            if (Objects.nonNull(podZabType)) {
                pamyatka.setPodZabType(podZabType);
                result = Optional.of(pamyatka);
                pamyatkaTableParser.parseVagonTables(pamyatka, rawFile);
            }
        }
        return result;
    }

    private boolean isNotEmpty(Pamyatka pamyatka) {
        return StringUtils.isNotEmpty(pamyatka.getNumber()) &&
                StringUtils.isNotEmpty(pamyatka.getType());
    }

    private PodZabType getPodZabType(String type) {
        PodZabType result;
        if (StringUtils.contains(type, parsingProps.getPodKeyword())) {
            result = PodZabType.POD;
        } else if (StringUtils.contains(type, parsingProps.getZabKeyword())) {
            result = PodZabType.ZAB;
        } else {
            result = null;
        }
        return result;
    }

    private boolean isPamyatkaNumberLine(String line) {
        return line.startsWith(parsingProps.getHeader());
    }

}
