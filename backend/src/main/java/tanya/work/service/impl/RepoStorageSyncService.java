package tanya.work.service.impl;

import com.google.common.base.Joiner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tanya.work.repo.impl.BaseCqEngineRepository;
import tanya.work.service.CloudStorageService;

import javax.annotation.PostConstruct;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static tanya.work.util.HostnameUtil.HOSTNAME;

@Slf4j
@Component
@RequiredArgsConstructor
public class RepoStorageSyncService {

    private static final String BASE_REPO_PATH = "repo";
    private static final String REPO_FILE_EXTENSION = ".dat";

    private final CloudStorageService cloudStorageService;
    private final List<BaseCqEngineRepository<?>> allRepos;

    private Map<String, AtomicLong> lastSavedChanges;

    @Scheduled(fixedDelayString = "${storage.repoSyncIntervalSec}", timeUnit = TimeUnit.SECONDS,
            initialDelayString = "${storage.repoSyncIntervalSec}")
    void persistRepositories() {
        initLastSavedChangesIfNeeded();
        for (BaseCqEngineRepository<?> repo : allRepos) {
            if (repo.isSerializable()) {
                persistRepository(repo);
            }
        }
    }

    @PostConstruct
    private void loadCloudData() {
        for (BaseCqEngineRepository<?> repo : allRepos) {
            if (repo.isSerializable()) {
                loadCloudData(repo);
            }
        }
    }

    private void loadCloudData(BaseCqEngineRepository<?> repo) {
        Optional<InputStream> repoData = cloudStorageService.getInputStream(getStorageUri(repo));
        if (repoData.isPresent()) {
            try(ObjectInputStream in = new ObjectInputStream(new GZIPInputStream(repoData.get()))) {
                List<Object> readObjects = readObjects(in);
                repo.loadData(readObjects);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private List<Object> readObjects(ObjectInputStream in) throws IOException {
        List<Object> result = new ArrayList<>();
        try {
            Object object;
            do {
                object = in.readObject();
                if (null != object) {
                    result.add(object);
                }
            } while (null != object);
        } catch (EOFException e) {
            log.debug("EOF reached");
        } catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
        return result;
    }

    private void persistRepository(BaseCqEngineRepository<?> repo) {
        AtomicLong lastSavedCounter = lastSavedChanges.get(repo.getRepoName());
        long repoChangesCount = repo.getChangesCount().get();

        if (repoChangesCount != lastSavedCounter.get() && repo.getAll().hasNext()) {
            cloudStorageService.save(getStorageUri(repo), out -> serializeEntities(repo.getAll(), out));
            lastSavedCounter.set(repoChangesCount);
        }
    }
    private void serializeEntities(Iterator<?> entities, OutputStream out) throws IOException {
        try(ObjectOutputStream objOut = new ObjectOutputStream(new GZIPOutputStream(out))) {
            while (entities.hasNext()) {
                objOut.writeObject(entities.next());
            }
        }
    }

    private void initLastSavedChangesIfNeeded() {
        if (null == lastSavedChanges) {
            lastSavedChanges = new HashMap<>();
            allRepos.forEach(repo -> lastSavedChanges.put(repo.getRepoName(), new AtomicLong()));
        }
    }

    private String getStorageUri(BaseCqEngineRepository<?> repo) {
        return Joiner.on('/').join(BASE_REPO_PATH, HOSTNAME,repo.getRepoName() + REPO_FILE_EXTENSION);
    }

}
