package tanya.work.service.impl;

import com.google.api.client.util.Objects;
import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import tanya.work.config.VagonParsingProperties;
import tanya.work.dto.PdfTableDto;
import tanya.work.dto.TableColumnDto;
import tanya.work.model.Pamyatka;
import tanya.work.model.RawFile;
import tanya.work.service.PamyatkaTableParser;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class PamyatkaTableParserImpl implements PamyatkaTableParser {
    private final VagonParsingProperties parsingProperties;
    private final PdfTableFacadeImpl pdfTableFacade;
    private Pattern vagonNumberPattern;

    @PostConstruct
    private void initialize() {
        vagonNumberPattern = parsingProperties.getNumberLength().toDigitsPattern();
    }

    @Override
    public void parseVagonTables(Pamyatka pamyatka, RawFile rawFile) {
        Preconditions.checkArgument(Objects.equal(pamyatka.getChecksum(), rawFile.getChecksum()));
        List<PdfTableDto> tables = pdfTableFacade.getPdfTables(rawFile);
        List<String> vagonNumbers = getVagonNumbers(tables);
        pamyatka.setVagonNumbers(vagonNumbers);
    }

    private List<String> getVagonNumbers(List<PdfTableDto> tables) {
        List<String> result = new ArrayList<>();
        for (PdfTableDto table : tables) {
            for (TableColumnDto column : table.getColumns()) {
                for (String columnValue : column.getData()) {
                    if (isVagonNumber(columnValue)) {
                        result.add(columnValue);
                    }
                }
            }
        }
        return result;
    }
    private boolean isVagonNumber(String value) {
        return StringUtils.isNotEmpty(value) && vagonNumberPattern.matcher(value).matches();
    }
}
