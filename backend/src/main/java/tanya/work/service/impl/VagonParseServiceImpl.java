package tanya.work.service.impl;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import tanya.work.config.VagonParsingProperties;
import tanya.work.model.ParsedFile;
import tanya.work.model.RawFile;
import tanya.work.model.Vagon;
import tanya.work.model.VagonTableColumn;
import tanya.work.repo.ParsedFileRepository;
import tanya.work.service.VagonParseService;
import tanya.work.service.VagonTableHeaderFinder;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static tanya.work.util.NumberUtils.parseInt;
import static tanya.work.util.poi.CellUtil.getCellValue;

@Slf4j
@Service
@RequiredArgsConstructor
public class VagonParseServiceImpl implements VagonParseService {

    private final VagonTableHeaderFinder vagonTableHeaderFinder;
    private final ParsedFileRepository parsedFileRepository;
    private final VagonParsingProperties parsingProps;
    private Pattern vagonNumberPattern;

    @PostConstruct
    private void initialize() {
        vagonNumberPattern = parsingProps.getNumberLength().toDigitsPattern();
    }

    @Override
    public Set<Vagon> parse(RawFile excelFile) {
        Preconditions.checkArgument(excelFile.isExcel(), excelFile.getName() + " is not an Excel file");
        long before = System.currentTimeMillis();
        Workbook workbook = openWorkbook(excelFile);
        Set<Vagon> result = parseWorkbook(workbook);
        result.forEach(vagon -> vagon.setSourceFile(excelFile.getStorageUri()));
        log.info("Parsed {} in {} ms", excelFile.getName(), System.currentTimeMillis() - before);
        parsedFileRepository.save(new ParsedFile(excelFile));
        return result;
    }

    private Set<Vagon> parseWorkbook(Workbook workbook) {
        Set<Vagon> result = new HashSet<>();
        boolean stopAfterFound = !parsingProps.isContinueAfterFoundSheet();
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);
            Optional<VagonTableHeader> vagonTableHeader = vagonTableHeaderFinder.findVagonTableHeader(sheet);
            if (vagonTableHeader.isPresent()) {
                Set<Vagon> vagonsFromHeader = parseSheetWithHeader(sheet, vagonTableHeader.get());
                result.addAll(vagonsFromHeader);
                if (CollectionUtils.isNotEmpty(vagonsFromHeader) && stopAfterFound) {
                    break;
                }
            } else if (!parsingProps.isMustFindHeader()) {
                Set<Vagon> vagonsFromRawTable = parseSheetWithoutHeader(sheet);
                result.addAll(vagonsFromRawTable);
                if (CollectionUtils.isNotEmpty(vagonsFromRawTable) && stopAfterFound) {
                    break;
                }
            }
        }
        return result;
    }

    private Set<Vagon> parseSheetWithHeader(Sheet sheet, VagonTableHeader tableHeader) {
        Set<Vagon> result = new HashSet<>();
        for (Row row : sheet) {
            if (row.getRowNum() > tableHeader.getRowNum()) {
                result.addAll(findAndCreateVagons(row, tableHeader.getColumnIndexes()));
            }
        }
        return result;
    }

    private Set<Vagon> parseSheetWithoutHeader(Sheet sheet) {
        Set<Vagon> result = new HashSet<>();
        Map<VagonTableColumn, Integer> defaultIndexes = toIntIndexes(parsingProps.getHeader().getDefaultIndexes());
        for (Row row : sheet) {
            result.addAll(findAndCreateVagons(row, defaultIndexes));
        }
        return result;
    }

    private Set<Vagon> findAndCreateVagons(Row row, Map<VagonTableColumn, Integer> columnIndexes) {
        Set<Vagon> result = Collections.emptySet();

        Map<VagonTableColumn, String> rowValues = getRowValues(row, columnIndexes);
        if (allRequiredValuesPresent(rowValues)) {
            String compositeVagonNumber = rowValues.remove(VagonTableColumn.N_VAGON);
            Preconditions.checkNotNull(compositeVagonNumber);
            result = createVagons(compositeVagonNumber, rowValues);
        }
        return result;
    }

    private Set<Vagon> createVagons(String compositeVagonNumber, Map<VagonTableColumn, String> rowValues) {
        Set<Vagon> result = new HashSet<>();
        for (String individualVagonNumber : splitVagonNumber(compositeVagonNumber)) {
            if (isVagonNumber(individualVagonNumber)) {
                Vagon vagon = new Vagon();
                vagon.setNumber(individualVagonNumber);
                rowValues.forEach((col, value) -> setVagonColumnValue(vagon, col, value));
                result.add(vagon);
            }
        }
        return result;
    }

    private Map<VagonTableColumn, String> getRowValues(Row row, Map<VagonTableColumn, Integer> columnIndexes) {
        Map<VagonTableColumn, String> result = new HashMap<>();
        for (Map.Entry<VagonTableColumn, Integer> entry : columnIndexes.entrySet()) {
            Cell cell = row.getCell(entry.getValue());
            String cellValue = getCellValue(cell);
            if (StringUtils.isNotEmpty(cellValue)) {
                result.put(entry.getKey(), cellValue);
            }
        }
        return result;
    }

    // N_VAGON is processed separately
    private void setVagonColumnValue(Vagon vagon, VagonTableColumn column, String value) {
        switch (column) {
            case N_PP -> parseInt(value, vagon::setNumPp);
            case FIX_TYPE -> vagon.setFixType(value);
            case OWNER -> vagon.setOwner(value);
            case YEAR_BUILT -> parseInt(value, vagon::setYearBuilt);
            case NOTES -> vagon.setNotes(value);
        }
    }

    private boolean allRequiredValuesPresent(Map<VagonTableColumn, String> rowValues) {
        List<VagonTableColumn> requiredColumns = parsingProps.getHeader().getRequired();
        return rowValues.keySet().containsAll(requiredColumns) &&
                rowValues.entrySet().stream().allMatch(e -> isValidRequiredValue(e.getKey(), e.getValue()));
    }

    private boolean isValidRequiredValue(VagonTableColumn column, String value) {
        return VagonTableColumn.N_VAGON.equals(column) ? isVagonNumber(value) : StringUtils.isNotEmpty(value);
    }

    private boolean isVagonNumber(String value) {
        return StringUtils.isNotEmpty(value) && vagonNumberPattern.matcher(value).find();
    }

    private Set<String> splitVagonNumber(String vagonNumber) {
        String[] split = vagonNumber.split("\\D+");
        return Arrays.stream(split).collect(Collectors.toSet());
    }

    private Map<VagonTableColumn, Integer> toIntIndexes(Map<VagonTableColumn, String> stringIndexes) {
        Map<VagonTableColumn, Integer> result = new HashMap<>();
        for (Map.Entry<VagonTableColumn, String> entry : stringIndexes.entrySet()) {
            CellAddress cellAddress = new CellAddress(entry.getValue() + "1"); // A -> A1, make valid cell addr
            result.put(entry.getKey(), cellAddress.getColumn());
        }
        return result;
    }

    private Workbook openWorkbook(RawFile rawFile) {
        Workbook result;
        String extension = rawFile.getExtension();
        InputStream content = new ByteArrayInputStream(rawFile.getContent());
        try {
            if ("xls".equals(extension)) {
                result = new HSSFWorkbook(content);
            } else if ("xlsx".equals(extension)) {
                result = new XSSFWorkbook(content);
            } else {
                throw new IllegalArgumentException("Unknown Excel extension " + extension);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return result;
    }

}
