package tanya.work.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;
import tanya.work.config.VagonParsingProperties;
import tanya.work.model.VagonTableColumn;
import tanya.work.service.VagonTableHeaderFinder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static tanya.work.util.poi.CellUtil.getCellValue;

@Slf4j
@Service
@RequiredArgsConstructor
public class VagonTableHeaderFinderImpl implements VagonTableHeaderFinder {

    private final VagonParsingProperties parsingProps;

    @Override
    public Optional<VagonTableHeader> findVagonTableHeader(Sheet sheet) {
        Optional<VagonTableHeader> result = Optional.empty();
        Map<VagonTableColumn, String> headerNames = parsingProps.getHeader().getNames();
        List<VagonTableColumn> requiredColumns = parsingProps.getHeader().getRequired();
        int searchRowsRange = parsingProps.getHeader().getSearchRowsRange();

        int i = 1;
        for (Row row : sheet) {
            if (i++ < searchRowsRange) {
                Optional<VagonTableHeader> foundHeader = findVagonTableHeader(row, headerNames, requiredColumns);
                if (foundHeader.isPresent()) {
                    result = foundHeader;
                    log.info("Found Vagon Table Header in sheet {} at row {}", sheet.getSheetName(), row.getRowNum());
                    break;
                }
            }
        }
        return result;
    }

    private Optional<VagonTableHeader> findVagonTableHeader(Row row, Map<VagonTableColumn, String> headerNames,
                                                       List<VagonTableColumn> requiredColumns) {
        VagonTableHeader matchedHeader = new VagonTableHeader();
        Map<VagonTableColumn, String> remainingNames = new HashMap<>(headerNames);
        for (Cell cell : row) {
            Optional<VagonTableColumn> foundColumn = findHeaderColumnByName(cell, remainingNames);
            foundColumn.ifPresent(col -> matchedHeader.addColumnIndex(col, cell.getColumnIndex()));
            matchedHeader.setRowNum(row.getRowNum());
        }
        return matchedHeader.containsAll(requiredColumns) ? Optional.of(matchedHeader) : Optional.empty();
    }

    private Optional<VagonTableColumn> findHeaderColumnByName(Cell cell, Map<VagonTableColumn, String> columnsNames) {
        Optional<VagonTableColumn> result = Optional.empty();
        String cellValue = getCellValue(cell);
        for (Map.Entry<VagonTableColumn, String> entry : columnsNames.entrySet()) {
            if (StringUtils.equalsIgnoreCase(cellValue, entry.getValue())) {
                result = Optional.of(entry.getKey());
                break;
            }
        }
        result.ifPresent(columnsNames::remove);
        return result;
    }
}
