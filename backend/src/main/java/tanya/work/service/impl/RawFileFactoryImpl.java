package tanya.work.service.impl;

import com.google.common.base.Joiner;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tanya.work.model.RawFile;
import tanya.work.service.RawFileFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class RawFileFactoryImpl implements RawFileFactory {

    @Override
    public RawFile create(MultipartFile file, String sessionId) {
        byte[] content = getContent(file);
        String name = file.getOriginalFilename();
        return new RawFile(
                content,
                file.getContentType(),
                name,
                getStorageUri(name, sessionId));
    }

    private byte[] getContent(MultipartFile file) {
        try {
            return file.getBytes();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private String getStorageUri(String name, String sessionId) {
        String currentDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        return Joiner.on('/').join(currentDate, sessionId, name);
    }

}
