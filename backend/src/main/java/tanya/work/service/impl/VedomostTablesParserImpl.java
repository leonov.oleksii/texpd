package tanya.work.service.impl;

import com.google.api.client.util.Objects;
import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import tanya.work.config.PamyatkaParsingProperties;
import tanya.work.dto.PdfTableDto;
import tanya.work.dto.TableColumnDto;
import tanya.work.model.PodZab;
import tanya.work.model.RawFile;
import tanya.work.model.Vedomost;
import tanya.work.service.PdfTableFacade;
import tanya.work.service.VedomostTablesParser;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class VedomostTablesParserImpl implements VedomostTablesParser {
    private final PdfTableFacade pdfTableFacade;
    private final PamyatkaParsingProperties parsingProperties;
    private Pattern pamyatkaNumberPattern;

    @PostConstruct
    private void initialize() {
        pamyatkaNumberPattern = parsingProperties.getNumberLength().toDigitsPattern();
    }

    @Override
    public void parsePamyatkaTables(Vedomost vedomost, RawFile rawFile) {
        Preconditions.checkArgument(Objects.equal(vedomost.getChecksum(), rawFile.getChecksum()));
        List<PdfTableDto> tables = pdfTableFacade.getPdfTables(rawFile);

        if (tables.size() == 1) {
            removeFirstDataRow(tables.get(0));
        } else if (tables.size() > 1) {
            fixPossibleMissingColumnName(tables);
        }

        vedomost.setPamyatkas(readPamyatkaNumbers(tables));
    }

    private PodZab<String> readPamyatkaNumbers(Collection<PdfTableDto> tables) {
        PodZab<String> result = new PodZab<>();
        for (PdfTableDto table : tables) {
            for (TableColumnDto column : table.getColumns()) {
                if (isPodColumn(column)) {
                    addPodZab(column, result.getPod());
                } else if (isZabColumn(column)) {
                    addPodZab(column, result.getZab());
                }
            }
        }
        return result;
    }

    private void addPodZab(TableColumnDto column, List<String> result) {
        for (String columnValue : column.getData()) {
            if (isPamyatkaNumber(columnValue)) {
                result.add(columnValue);
            }
        }
    }

    private void fixPossibleMissingColumnName(List<PdfTableDto> tables) {
        Iterator<PdfTableDto> iterator = tables.iterator();
        PdfTableDto firstTable = iterator.next();
        Map<String, TableColumnDto> firstTableColumnsByIndex = new LinkedHashMap<>();
        for (TableColumnDto column : firstTable.getColumns()) {
            if (CollectionUtils.isNotEmpty(column.getData())) {
                firstTableColumnsByIndex.put(column.getData().remove(0), column);
            }
        }

        while (iterator.hasNext()) {
            PdfTableDto nextTable = iterator.next();
            fixPossibleMissingColumnName(nextTable, firstTableColumnsByIndex);
        }
    }

    private void fixPossibleMissingColumnName(PdfTableDto nextTable,
                                              Map<String, TableColumnDto> firstTableColumnsByIndex) {
        for (TableColumnDto column : nextTable.getColumns()) {
            if (firstTableColumnsByIndex.containsKey(column.getName())) {
                TableColumnDto firstTableColumn = firstTableColumnsByIndex.get(column.getName());
                column.setName(firstTableColumn.getName());
            }
        }
    }

    private void removeFirstDataRow(PdfTableDto pdfTable) {
        for (TableColumnDto column : pdfTable.getColumns()) {
            if (CollectionUtils.isNotEmpty(column.getData())) {
                column.getData().remove(0);
            }
        }
    }

    private boolean isPodColumn(TableColumnDto column) {
        return StringUtils.containsIgnoreCase(column.getName(), parsingProperties.getNumKeyword()) &&
                StringUtils.containsIgnoreCase(column.getName(), parsingProperties.getPodKeyword());
    }

    private boolean isZabColumn(TableColumnDto column) {
        return StringUtils.containsIgnoreCase(column.getName(), parsingProperties.getNumKeyword()) &&
                StringUtils.containsIgnoreCase(column.getName(), parsingProperties.getZabKeyword());
    }

    private boolean isPamyatkaNumber(String value) {
        return StringUtils.isNotEmpty(value) && pamyatkaNumberPattern.matcher(value).matches();
    }
}
