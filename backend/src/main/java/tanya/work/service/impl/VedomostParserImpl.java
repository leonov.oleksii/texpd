package tanya.work.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import tanya.work.config.PamyatkaParsingProperties;
import tanya.work.config.VedomostParsingProperties;
import tanya.work.model.PdfFile;
import tanya.work.model.RawFile;
import tanya.work.model.Vedomost;
import tanya.work.service.VedomostParser;
import tanya.work.service.VedomostTablesParser;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static tanya.work.util.LineUtils.extractOneNumber;
import static tanya.work.util.LineUtils.getLine;

@Slf4j
@Service
@RequiredArgsConstructor
public class VedomostParserImpl implements VedomostParser {
    private static final Pattern DIGITS_2_GROUPS = Pattern.compile("^\\D*?(\\d+\\D*?\\d+)\\D*");
    private static final Pattern PRICE = Pattern.compile("^\\D*?(\\d+,?\\d*).*");
    private static final Locale UA_LOCALE = Locale.forLanguageTag("uk-UA");
    private static final DateTimeFormatter UA_DATE_FORMAT = DateTimeFormatter.ofPattern("d MMMM yyyy", UA_LOCALE);
    private static final NumberFormat UA_NUMBER_FORMAT = NumberFormat.getInstance(UA_LOCALE);

    private final VedomostTablesParser vedomostTablesParser;
    private final PamyatkaParsingProperties pamyatkaProperties;
    private final VedomostParsingProperties vedomostProperties;

    @Override
    public Optional<Vedomost> parse(PdfFile pdfFile, RawFile rawFile) {
        Vedomost vedomost = new Vedomost(pdfFile);
        Optional<Vedomost> result = Optional.empty();
        List<String> textContent = pdfFile.getTextContent();
        for (int i = 0; i < textContent.size(); i++) {
            String line = textContent.get(i);
            if (isVedomostNumberLine(line)) {
                vedomost.setNumber(extractOneNumber(line));
                vedomost.setType(getLine(textContent, i + 1));
                if (isPodZab(vedomost.getType())) {
                    vedomost.setPodZab(true);
                    vedomost.setDate(extractDate(getLine(textContent, i + 2)));
                }
            } else if (isVedomostPriceLine(line)) {
                vedomost.setPrice(extractPrice(line));
            }
        }
        if (isNotEmpty(vedomost) && vedomost.isPodZab()) {
            result = Optional.of(vedomost);
            vedomostTablesParser.parsePamyatkaTables(vedomost, rawFile);
        }
        return result;
    }

    private LocalDate extractDate(String line) {
        LocalDate result = null;
        Matcher matcher = DIGITS_2_GROUPS.matcher(line);
        if (matcher.matches()) {
            result = LocalDate.parse(matcher.group(1), UA_DATE_FORMAT);
        }
        return result;
    }

    private BigDecimal extractPrice(String line) {
        BigDecimal result = null;
        Matcher matcher = PRICE.matcher(line);
        if (matcher.matches()) {
            try {
                Number number = UA_NUMBER_FORMAT.parse(matcher.group(1));
                result = BigDecimal
                        .valueOf(number.doubleValue())
                        .setScale(2, RoundingMode.HALF_UP);
            } catch (ParseException e) {
                log.warn("Cannot extract price from line: '{}'", line);
            }

        }
        return result;
    }

    private boolean isPodZab(String vedomostType) {
        String zabKeyword = pamyatkaProperties.getZabKeyword();
        String podKeyword = pamyatkaProperties.getPodKeyword();
        return StringUtils.isNotEmpty(vedomostType)
                && vedomostType.contains(podKeyword)
                && vedomostType.contains(zabKeyword);
    }

    private boolean isNotEmpty(Vedomost vedomost) {
        return StringUtils.isNotEmpty(vedomost.getNumber()) &&
                StringUtils.isNotEmpty(vedomost.getType()) &&
                Objects.nonNull(vedomost.getDate()) &&
                Objects.nonNull(vedomost.getPrice());
    }

    private boolean isVedomostNumberLine(String line) {
        return line.startsWith(vedomostProperties.getHeader());
    }

    private boolean isVedomostPriceLine(String line) {
        return line.startsWith(vedomostProperties.getPriceLine());
    }

}
