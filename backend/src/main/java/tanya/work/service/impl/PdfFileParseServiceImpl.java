package tanya.work.service.impl;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tanya.work.model.PdfFile;
import tanya.work.model.RawFile;
import tanya.work.service.PdfFileParseService;
import tanya.work.service.PdfTextExtractor;

@Slf4j
@Service
@RequiredArgsConstructor
public class PdfFileParseServiceImpl implements PdfFileParseService {
    private final PdfTextExtractor pdfTextExtractor;

    @Override
    public PdfFile parse(RawFile rawFile) {
        Preconditions.checkArgument(rawFile.isPdf(), rawFile.getName() + " is not a PDF file");
        PdfFile pdfFile = new PdfFile(rawFile);
        pdfFile.setTextContent(pdfTextExtractor.extractText(rawFile));
        return pdfFile;
    }

}
