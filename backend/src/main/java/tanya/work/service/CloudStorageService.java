package tanya.work.service;

import tanya.work.model.RawFile;
import tanya.work.util.io.OutputStreamConsumer;

import java.io.InputStream;
import java.util.Optional;

public interface CloudStorageService {
    void save(RawFile file);
    void save(String storageUri, OutputStreamConsumer streamConsumer);
    Optional<InputStream> getInputStream(String storageUri);
}
