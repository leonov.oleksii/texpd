package tanya.work.service;

import tanya.work.model.PdfFile;
import tanya.work.model.RawFile;

public interface PdfFileParseService {
    PdfFile parse(RawFile rawFile);
}
