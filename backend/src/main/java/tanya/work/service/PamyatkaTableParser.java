package tanya.work.service;

import tanya.work.model.Pamyatka;
import tanya.work.model.RawFile;

public interface PamyatkaTableParser {
    void parseVagonTables(Pamyatka pamyatka, RawFile rawFile);
}
