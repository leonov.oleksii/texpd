package tanya.work.service;

import tanya.work.model.PdfFile;
import tanya.work.model.RawFile;
import tanya.work.model.Vedomost;

import java.util.Optional;

public interface VedomostParser {
    Optional<Vedomost> parse(PdfFile pdfFile, RawFile rawFile);
}
