package tanya.work.service;

import tanya.work.model.RawFile;
import tanya.work.model.Vedomost;

public interface VedomostTablesParser {
    void parsePamyatkaTables(Vedomost vedomost, RawFile rawFile);
}
