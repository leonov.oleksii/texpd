package tanya.work;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class TexPd {

    public static void main(String[] args) {
        SpringApplication.run(TexPd.class, args);
    }
}
