package tanya.work.repo;

import tanya.work.model.RawFile;

import java.util.Optional;

public interface RawFileRepository extends Repository<RawFile> {
    Optional<RawFile> findByChecksum(String checksum);
    void saveFor(RawFile rawFile, Runnable action);
}
