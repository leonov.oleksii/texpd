package tanya.work.repo.impl;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.index.Index;
import com.googlecode.cqengine.index.unique.UniqueIndex;
import org.springframework.stereotype.Repository;
import tanya.work.model.PdfFile;
import tanya.work.repo.PdfFileRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static tanya.work.util.cq.FunctionalAttribute.checksumAttr;

@Repository
public class PdfFileRepositoryImpl extends BaseCqEngineRepository<PdfFile> implements PdfFileRepository {
    private Attribute<PdfFile, String> checksumAttr;

    @Override
    protected Collection<Index<PdfFile>> initIndexes() {
        checksumAttr = checksumAttr(PdfFile.class);
        return Collections.singleton(UniqueIndex.onAttribute(checksumAttr));
    }

    @Override
    public Optional<PdfFile> findByChecksum(String checksum) {
        return findOneByEqual(checksumAttr, checksum);
    }

}
