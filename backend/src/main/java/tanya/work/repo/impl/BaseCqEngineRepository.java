package tanya.work.repo.impl;

import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.index.Index;
import com.googlecode.cqengine.query.QueryFactory;
import com.googlecode.cqengine.resultset.ResultSet;
import lombok.Getter;
import tanya.work.repo.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public abstract class BaseCqEngineRepository<T> implements Repository<T> {

    @Getter
    private final AtomicLong changesCount = new AtomicLong();
    @Getter
    private final Class<T> entityClass;

    protected final IndexedCollection<T> data;

    public BaseCqEngineRepository() {
        this.entityClass = findFirstTypeArgument();
        data = new ConcurrentIndexedCollection<>();
        for (Index<T> index : initIndexes()) {
            data.addIndex(index);
        }
    }

    @Override
    public void save(T entity) {
        data.add(entity);
        countChanges();
    }

    public void delete(T entity) {
        data.remove(entity);
        countChanges();
    }

    @SuppressWarnings("unchecked")
    public void loadData(List<?> cloudData) {
        data.clear();
        data.update(Collections.emptySet(), (List<T>) cloudData);
    }

    public <A> Optional<T> findOneByEqual(Attribute<T, A> attribute, A attributeValue) {
        ResultSet<T> resultSet = data.retrieve(QueryFactory.equal(attribute, attributeValue));
        return Optional.ofNullable(resultSet.isNotEmpty() ? resultSet.iterator().next() : null);
    }

    public Iterator<T> getAll() {
        return data.iterator();
    }

    public String getRepoName() {
        return entityClass.getSimpleName();
    }

    protected abstract Collection<Index<T>> initIndexes();

    protected final void countChanges() {
        changesCount.incrementAndGet();
    }

    @SuppressWarnings("unchecked")
    private Class<T> findFirstTypeArgument() {
        ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    public boolean isSerializable() {
        return Serializable.class.isAssignableFrom(entityClass);
    }
}
