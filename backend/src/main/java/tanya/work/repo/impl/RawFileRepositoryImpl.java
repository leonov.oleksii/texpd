package tanya.work.repo.impl;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.index.Index;
import com.googlecode.cqengine.index.unique.UniqueIndex;
import org.springframework.stereotype.Repository;
import tanya.work.model.RawFile;
import tanya.work.repo.RawFileRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static tanya.work.util.cq.FunctionalAttribute.checksumAttr;

@Repository
public class RawFileRepositoryImpl extends BaseCqEngineRepository<RawFile> implements RawFileRepository  {
    private Attribute<RawFile, String> checksumAttr;

    @Override
    protected Collection<Index<RawFile>> initIndexes() {
        checksumAttr = checksumAttr(RawFile.class);
        return Collections.singleton(UniqueIndex.onAttribute(checksumAttr));
    }

    @Override
    public Optional<RawFile> findByChecksum(String checksum) {
        return findOneByEqual(checksumAttr, checksum);
    }

    @Override
    public void saveFor(RawFile rawFile, Runnable action) {
        save(rawFile);
        try {
            action.run();
        } finally {
            delete(rawFile);
        }
    }
}
