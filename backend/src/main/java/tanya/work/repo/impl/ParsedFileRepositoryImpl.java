package tanya.work.repo.impl;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.index.Index;
import com.googlecode.cqengine.index.unique.UniqueIndex;
import org.springframework.stereotype.Repository;
import tanya.work.model.ParsedFile;
import tanya.work.repo.ParsedFileRepository;

import java.util.Collection;
import java.util.Collections;

import static tanya.work.util.cq.FunctionalAttribute.checksumAttr;

@Repository
public class ParsedFileRepositoryImpl extends BaseCqEngineRepository<ParsedFile> implements ParsedFileRepository {
    private Attribute<ParsedFile, String> checksumAttr;

    @Override
    protected Collection<Index<ParsedFile>> initIndexes() {
        checksumAttr = checksumAttr(ParsedFile.class);
        return Collections.singleton(UniqueIndex.onAttribute(checksumAttr));
    }

    @Override
    public boolean existByChecksum(String checksum) {
        return findOneByEqual(checksumAttr, checksum).isPresent();
    }


}
