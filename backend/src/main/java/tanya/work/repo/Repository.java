package tanya.work.repo;

import com.googlecode.cqengine.attribute.Attribute;

import java.util.Iterator;
import java.util.Optional;

public interface Repository<T> {
    void save(T entity);
    void delete(T entity);
    <A> Optional<T> findOneByEqual(Attribute<T, A> attribute, A attributeValue);
    Iterator<T> getAll();
}
