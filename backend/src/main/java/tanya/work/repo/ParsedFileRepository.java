package tanya.work.repo;

import tanya.work.model.ParsedFile;

public interface ParsedFileRepository extends Repository<ParsedFile> {
    boolean existByChecksum(String checksum);
}
