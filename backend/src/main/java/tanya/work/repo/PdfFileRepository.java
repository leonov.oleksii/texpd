package tanya.work.repo;

import tanya.work.model.PdfFile;

import java.util.Optional;

public interface PdfFileRepository extends Repository<PdfFile> {
    Optional<PdfFile> findByChecksum(String checksum);
}
