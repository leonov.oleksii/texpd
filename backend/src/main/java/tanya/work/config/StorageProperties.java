package tanya.work.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "storage")
public class StorageProperties {
    private long repoSyncIntervalSec;
    private int saveThreadCount;
    private String bucketName;
    private List<String> keyPaths;
}
