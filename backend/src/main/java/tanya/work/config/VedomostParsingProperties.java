package tanya.work.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "vedomost-parsing")
public class VedomostParsingProperties {
    private String header;
    private String priceLine;
}