package tanya.work.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "pamyatka-parsing")
public class PamyatkaParsingProperties {
    private String header;
    private String podKeyword;
    private String zabKeyword;
    private String numKeyword;
    @NestedConfigurationProperty
    private NumberRange numberLength;
}