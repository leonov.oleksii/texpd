package tanya.work.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;
import tanya.work.model.VagonTableColumn;

import java.util.List;
import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "vagon-parsing")
public class VagonParsingProperties {
    private boolean continueAfterFoundSheet;
    private boolean mustFindHeader;
    @NestedConfigurationProperty
    private NumberRange numberLength;
    private HeaderProps header;

    @Data
    public static class HeaderProps {
        private int searchRowsRange;
        private Map<VagonTableColumn, String> names;
        private List<VagonTableColumn> required;
        private Map<VagonTableColumn, String> defaultIndexes;
    }

}