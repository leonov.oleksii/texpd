package tanya.work.config;

import com.google.common.base.Preconditions;
import lombok.Data;

import java.util.regex.Pattern;

@Data
public class NumberRange {
    private int min;
    private int max;

    public Pattern toDigitsPattern() {
        Preconditions.checkState(max >= min);
        return Pattern.compile(String.format("\\d{%d,%d}", min, max));
    }

}