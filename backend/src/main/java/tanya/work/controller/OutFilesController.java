package tanya.work.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tanya.work.model.RawFile;
import tanya.work.repo.RawFileRepository;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/out/files")
@RequiredArgsConstructor
public class OutFilesController {

    private final RawFileRepository rawFileRepository;

    @GetMapping("/{checksum}")
    public ResponseEntity<ByteArrayResource> getFile(@PathVariable String checksum) {
        ResponseEntity<ByteArrayResource> response;

        Optional<RawFile> foundFile = rawFileRepository.findByChecksum(checksum);
        if (foundFile.isEmpty()) {
            response = ResponseEntity.notFound().build();
        } else {
            RawFile file = foundFile.get();
            final ByteArrayResource res = new ByteArrayResource(file.getContent());
            response = ResponseEntity.ok().contentType(MediaType.parseMediaType(file.getContentType())).body(res);
        }
        return response;
    }
}
