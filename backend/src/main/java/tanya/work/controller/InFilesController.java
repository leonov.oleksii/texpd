package tanya.work.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tanya.work.model.Pamyatka;
import tanya.work.model.PdfFile;
import tanya.work.model.RawFile;
import tanya.work.repo.RawFileRepository;
import tanya.work.service.PamyatkaParser;
import tanya.work.service.PdfFileParseService;
import tanya.work.service.RawFileFactory;
import tanya.work.service.VagonParseService;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/in/files")
@RequiredArgsConstructor
public class InFilesController {
    private final RawFileFactory rawFileFactory;
    private final VagonParseService vagonParseService;
    private final PdfFileParseService pdfFileParseService;
    private final RawFileRepository rawFileRepository;
    //private final VedomostParser vedomostParser;
    private final PamyatkaParser pamyatkaParser;

    @PostMapping("/{sessionId}")
    public Pamyatka uploadFile(@PathVariable String sessionId, MultipartFile file) {
        Optional<Pamyatka> result = Optional.empty();
        RawFile rawFile = rawFileFactory.create(file, sessionId);
        rawFileRepository.save(rawFile);
        //fileStorageService.save(rawFile);
        if (rawFile.isPdf()) {
            PdfFile pdfFile = pdfFileParseService.parse(rawFile);
            // vedomost = vedomostParser.parse(pdfFile, rawFile);
            result = pamyatkaParser.parse(pdfFile, rawFile);
        } else {
            vagonParseService.parse(rawFile);
        }
        return result.orElse(null);
//        if (parsedFileRepository.existByChecksum(rawFile.getChecksum())) {
//            log.info("Exist by checksum: {}", rawFile.getChecksum());
//        }
    }

}
