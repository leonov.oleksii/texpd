package tanya.work.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PdfTableDto {
    private List<TableColumnDto> columns;
}
