package tanya.work.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

@Getter
@Setter
public class TableColumnDto {
    private String name;
    private List<String> data;

    @Override
    public String toString() {
        return new StringJoiner(", ", TableColumnDto.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("data size=" + CollectionUtils.size(data))
                .toString();
    }
}
